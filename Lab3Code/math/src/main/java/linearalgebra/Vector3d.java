package linearalgebra;

/**
 * @author Serhiy Fedurtsya
 */
public class Vector3d {
    private final double x, y, z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public double magnitude() {
        System.out.println(Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2)));
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    public double dotProduct(Vector3d vector3d) {
        System.out.println((x * vector3d.x) + (y * vector3d.y) + (z * vector3d.z));
        return (x * vector3d.x) + (y * vector3d.y) + (z * vector3d.z);
    }

    public Vector3d add(Vector3d vector3d) {
        return new Vector3d(x + vector3d.x, y + vector3d.y, z + vector3d.z);
    }
}