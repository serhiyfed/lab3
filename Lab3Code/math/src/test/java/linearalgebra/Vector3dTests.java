package linearalgebra;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * @author Serhiy Fedurtsya
 */
public class Vector3dTests {
    double x = 2;
    double y = 9;
    double z = 19;

    @Test
    public void testCreateVector3d() {
        Vector3d vector3d = new Vector3d(x, y, z);
        //assertTrue(vector3d.getX() == 1);
        assertTrue(x == vector3d.getX() && y == vector3d.getY() && z == vector3d.getZ());
    }

    @Test
    public void testMagnitude() {
        Vector3d vector3d = new Vector3d(x, y, z);
        assertTrue(vector3d.magnitude() == Math.sqrt(446));
    }

    @Test
    public void testDotProduct() {
        Vector3d vector3d = new Vector3d(x, y, z);
        Vector3d vector3d2 = new Vector3d(y, z, x);
        assertTrue(vector3d.dotProduct(vector3d2) == 227);
    }

    @Test
    public void testAdd() {
        Vector3d vector3d = new Vector3d(x, y, z);
        Vector3d vector3d2 = new Vector3d(y, z, x);
        Vector3d vector3d3 = vector3d.add(vector3d2);
        assertTrue(vector3d3.getX() == 11 && vector3d3.getY() == 28 && vector3d3.getZ() == 21);
    }
}